import json
import logging
import os
import shutil
import threading
import time
import urllib.request
from enum import Enum
from urllib.error import URLError

import settings

logger = logging.getLogger(__name__)


class ScreenshotType(Enum):
  UNKNOWN = 0
  BLACK = 1
  SOLID_COLOR = 2
  INFORMATION = 3
  SONG = 4
  SCRIPTURE = 5
  PRESENTATION = 6
  VIDEO = 7
  DESKTOP = 8


class Screenshot:
  def __init__(self, id: int, location: str, type: ScreenshotType):
    self.id = id
    self.location = location
    self.type = type

  def __repr__(self):
    return "Screenshot(id={};location={};type={})".format(self.id, self.location, self.type)


class ScreenshotService:
  def __init__(self) -> None:
    self.current_screenshot = None
    self.new_screenshot = None
    self.new_screenshot_location = 'media/screenshot_local_new.png'
    self.timer = None
    self.is_online = False

  def start_timer(self) -> None:
    self.timer = threading.Timer(settings.screenshots_update_interval, self.fetch_new_screenshot)
    self.timer.setDaemon(True)  # To prevent holding the system up on exit
    self.timer.start()

  def kick_timer(self) -> None:
    if self.timer is not None and self.timer.is_alive():
      return

    self.start_timer()

  def get_screenshot(self) -> Screenshot:
    logger.debug("Get screenshot's location")
    self.kick_timer()

    if self.is_new_screenshot_available():
      self.load_new_screenshot()

    return self.current_screenshot

  def load_new_screenshot(self) -> None:
    logger.debug("Loading new screenshot")
    if not os.path.exists(self.new_screenshot_location):
      return

    shutil.move(self.new_screenshot_location, settings.screenshots_local_location)
    self.current_screenshot = Screenshot(id=self.new_screenshot.id,
                                         location=settings.screenshots_local_location,
                                         type=self.new_screenshot.type)

  def is_new_screenshot_available(self, ) -> bool:
    logger.debug("Checking if there's a new screenshot available")
    if self.new_screenshot is None:
      return False

    if self.current_screenshot is None:
      return True

    return self.current_screenshot.id != self.new_screenshot.id

  def fetch_new_screenshot(self) -> None:
    logger.debug("Fetching new screenshot data")
    try:
      response = urllib.request.urlopen(settings.screenshots_info_URL)
      self.is_online = True
    except (ConnectionRefusedError, URLError, Exception) as e:
      if isinstance(e, ConnectionRefusedError) or isinstance(e, URLError):
        logger.exception(e, exc_info=False)
      else:
        logger.exception(e, exc_info=True)
      self.is_online = False
      self.start_timer()
      return

    json_data = json.loads(response.read().decode())

    if self.current_screenshot is not None and self.current_screenshot.id == json_data.get('id'):
      return

    logging.debug("New screenshot information: {}".format(json_data))
    if self.download_screenshot(json_data.get('id')):
      self.new_screenshot = Screenshot(id=json_data.get('id'),
                                       location=self.new_screenshot_location,
                                       type=ScreenshotType[json_data.get('type')])

  def download_screenshot(self, screenshot_id: int) -> bool:
    download_url = settings.screenshots_media_URL.format(id=screenshot_id)
    logger.debug("Download new screenshot from: {}".format(download_url))

    try:
      urllib.request.urlretrieve(download_url, self.new_screenshot_location)
      self.is_online = True
    except Exception as e:
      logger.exception(e, exc_info=True)
      self.is_online = False
      self.start_timer()
      return False

    logger.debug("Done downloading screenshot")
    return True


if __name__ == '__main__':
  screenshotService = ScreenshotService()
  logger.debug(screenshotService.new_screenshot.id)
  logger.debug(screenshotService.get_screenshot())
  time.sleep(10)
  logger.debug(screenshotService.get_screenshot())
