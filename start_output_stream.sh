#!/bin/bash

#IP="127.0.0.1"
IP="192.168.88.31"
PORT="5000"
OUTPUT_URL="rtsp://${IP}:${PORT}/stream.sdp"

FRAMERATE=25
WIDTH=$(python -c "import settings; print(settings.output_format.get('width', 640))")
HEIGHT=$(python -c "import settings; print(settings.output_format.get('height', 480))")

echo "Started streaming on: ${OUTPUT_URL}"
echo ""
python app.py --output=stdout | cvlc --demux=rawvideo --rawvid-fps=${FRAMERATE} --rawvid-width="${WIDTH}" --rawvid-height="${HEIGHT}" --rawvid-chroma=RV24 --rtsp-host=${IP} - --sout "#transcode{vcodec=h264,vb=200,fps=${FRAMERATE},width=${WIDTH},height=${HEIGHT}}:rtp{dst=${IP},port=${PORT},sdp=${OUTPUT_URL}"
