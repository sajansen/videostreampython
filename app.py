import logging
import sys
import time

import cv2
import numpy as np

import settings
from screenshots import ScreenshotService, ScreenshotType, Screenshot
from utils import Bounds

logger = logging.getLogger(__name__)

screenshotService = ScreenshotService()
overlay_bounds = Bounds(**settings.overlay_settings)
current_screenshot_id: int = None
current_screenshot_image = None
downtime_image = None
last_video_down_starttime = None
last_valid_frame = None
output_window_title = 'Stream'
VIDEO_OUTPUT: str = 'window'


def main(args: list) -> None:
  handle_command_args(args)
  screenshotService.start_timer()
  video_capturing()


def handle_command_args(args: list):
  global VIDEO_OUTPUT
  if '--output=stdout' in args:
    VIDEO_OUTPUT = 'stdout'
  else:
    VIDEO_OUTPUT = 'window'


def video_capturing() -> None:
  global last_valid_frame, last_video_down_starttime, downtime_image
  logger.info("Opening video capture...")
  video_capture = connect_to_stream(settings.streamURL.format(**settings.stream_connection_settings))

  logger.info("Starting video processing...")
  while True:
    ret, frame = video_capture.read()
    if not ret or frame is None:
      logger.warning("Failed to retrieve video frame")
      handle_video_down()
      continue
    elif last_video_down_starttime is not None:
      logger.info("Video restored")
      last_video_down_starttime = None
      downtime_image = None

    handle_video_frame(frame, delay=1)
    last_valid_frame = frame


def connect_to_stream(url: str):
  logger.debug("Connecting to: '{}'".format(url))
  video_capture = cv2.VideoCapture(url)
  while not video_capture.isOpened():
    logger.warning("Connection to stream failed. Retrying...")
    time.sleep(1)
    video_capture.open(url)
  return video_capture


def handle_video_down():
  global last_video_down_starttime, current_screenshot_id, current_screenshot_image, downtime_image, last_valid_frame

  if last_video_down_starttime is None:
    last_video_down_starttime = time.time()

  if last_valid_frame is not None and last_video_down_starttime + settings.max_downtime_timeout > time.time():
    # Do nothing, wait till downtime timeout has completed
    logger.debug("Waiting for downtime timer to cool down")
    handle_video_frame(last_valid_frame, delay=30)
    return

  logger.debug("Handling downtime image")
  if downtime_image is None:
    if last_valid_frame is None:
      downtime_image = np.zeros(shape=[512, 512, 3], dtype=np.uint8)
    else:
      downtime_image = cv2.blur(last_valid_frame, (50, 50))

  handle_video_frame(downtime_image, delay=30)
  cv2.displayOverlay(output_window_title,
                     'Stream down for {} seconds'.format(round(time.time() - last_video_down_starttime)), delayms=100)


def handle_video_frame(image, delay: int = 0):
  global current_screenshot_image, current_screenshot_id

  image = resize_image_to_max_box(image, width=settings.output_format.get("width", 640),
                                  height=settings.output_format.get("height", 480),
                                  ratio=settings.stream_video_desired_ratio,
                                  border=True)

  add_screenshot_overlay(image)

  show_image(image, delay=delay)


def add_screenshot_overlay(image):
  global current_screenshot_id, current_screenshot_image

  if not settings.add_screenshot_overlay or not screenshotService.is_online:
    return

  screenshot: Screenshot = screenshotService.get_screenshot()

  if screenshot is None:
    return

  if screenshot.type in [ScreenshotType.BLACK, ScreenshotType.SOLID_COLOR]:
    if current_screenshot_id != screenshot.id:
      current_screenshot_id = screenshot.id
      current_screenshot_image = None
      logging.debug("Hiding screenshot: {}".format(screenshot))
    return

  load_new_screenshot_image(screenshot)
  add_image_to_image(base_image=image, overlay_image=current_screenshot_image, bounds=overlay_bounds)


def load_new_screenshot_image(screenshot: Screenshot):
  global current_screenshot_id, current_screenshot_image

  if current_screenshot_id == screenshot.id and current_screenshot_image is not None:
    return

  current_screenshot_id = screenshot.id
  current_screenshot_image = cv2.imread(screenshot.location)
  current_screenshot_image = resize_image_to_max_box(current_screenshot_image,
                                                     width=overlay_bounds.width,
                                                     height=overlay_bounds.height)
  overlay_bounds.height, overlay_bounds.width = current_screenshot_image.shape[:2]


def add_image_to_image(base_image, overlay_image, bounds: Bounds) -> None:
  if base_image is None or overlay_image is None or bounds is None:
    raise ValueError("Missing required arguments")

  if bounds.x + bounds.width > base_image.shape[1] or bounds.y + bounds.height > base_image.shape[0]:
    raise ValueError("Base image out of bounds")

  base_image[bounds.y:bounds.y + bounds.height, bounds.x:bounds.x + bounds.width] = overlay_image


def resize_image_to_max_box(image, width: int, height: int, ratio: float = 1, border: bool = False):
  old_height, old_width = image.shape[:2]

  # Process ratio
  if ratio != 1:
    ratio_width = round(old_width * ratio)
    ratio_height = old_height
    if ratio_width < old_width:
      ratio_width = old_width
      ratio_height = round(old_height / ratio)
  else:
    ratio_width = old_width
    ratio_height = old_height

  # Process box
  ratio = height / ratio_height
  new_height = height
  new_width = round(ratio_width * ratio)
  if new_width > width:
    ratio = width / ratio_width
    new_width = width
    new_height = round(ratio_height * ratio)

  logger.debug("Resizing image from {}x{} to {}x{}".format(old_width, old_height, new_width, new_height))

  new_image = cv2.resize(image, (new_width, new_height), interpolation=cv2.INTER_AREA)

  if border:
    if new_width < width:
      border_width = round((width - new_width) / 2)
      new_image = cv2.copyMakeBorder(new_image, 0, 0, border_width, border_width, cv2.BORDER_CONSTANT, value=[0, 0, 0])
    if new_height < height:
      border_heigth = round((height - new_height) / 2)
      new_image = cv2.copyMakeBorder(new_image, border_heigth, border_heigth, 0, 0, cv2.BORDER_CONSTANT,
                                     value=[0, 0, 0])

  return new_image


def show_image(image, title: str = output_window_title, delay: int = 1) -> None:
  if VIDEO_OUTPUT == 'stdout':
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    sys.stdout.buffer.write(image.tobytes())
  else:
    cv2.namedWindow(title, cv2.WND_PROP_FULLSCREEN)
    cv2.moveWindow(title, settings.output_format.get('x', 0), settings.output_format.get('y', 0))
    cv2.setWindowProperty(title, cv2.WND_PROP_FULLSCREEN, cv2.WINDOW_FULLSCREEN)
    cv2.imshow(title, image)

  key = cv2.waitKey(delay)
  if key in [27, 13, 32]:
    sys.exit()


if __name__ == "__main__":
  main(sys.argv[1:])
