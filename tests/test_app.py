import unittest

import app
import numpy as np

from utils import Bounds


class AppTest(unittest.TestCase):

  def setUp(self) -> None:
    app.screenshotService = None

  def test_resize_image_to_max_box_1(self):
    image = np.zeros(shape=(100, 10, 3), dtype=np.uint8)
    new_image = app.resize_image_to_max_box(image, 150, 150, border=False)
    self.assertEqual((150, 15), new_image.shape[:2])

  def test_resize_image_to_max_box_2(self):
    image = np.zeros(shape=(10, 100, 3), dtype=np.uint8)
    new_image = app.resize_image_to_max_box(image, 150, 150, border=False)
    self.assertEqual((15, 150), new_image.shape[:2])

  def test_resize_image_to_max_box_with_same_height_but_extra_width_room(self):
    image = np.zeros(shape=(100, 10, 3), dtype=np.uint8)
    new_image = app.resize_image_to_max_box(image, 150, 100, border=False)
    self.assertEqual((100, 10), new_image.shape[:2])

  def test_resize_image_to_max_box_with_same_height_but_less_width_room(self):
    image = np.zeros(shape=(100, 10, 3), dtype=np.uint8)
    new_image = app.resize_image_to_max_box(image, 80, 100, border=False)
    self.assertEqual((100, 10), new_image.shape[:2])

  def test_resize_image_to_max_box_with_same_height_but_to_less_width_room(self):
    image = np.zeros(shape=(100, 50, 3), dtype=np.uint8)
    new_image = app.resize_image_to_max_box(image, 30, 100, border=False)
    self.assertEqual((60, 30), new_image.shape[:2])

  def test_resize_image_to_max_box_with_same_width_but_extra_height_room(self):
    image = np.zeros(shape=(10, 100, 3), dtype=np.uint8)
    new_image = app.resize_image_to_max_box(image, 100, 150, border=False)
    self.assertEqual((10, 100), new_image.shape[:2])

  def test_resize_image_to_max_box_with_same_width_but_less_height_room(self):
    image = np.zeros(shape=(10, 100, 3), dtype=np.uint8)
    new_image = app.resize_image_to_max_box(image, 100, 80, border=False)
    self.assertEqual((10, 100), new_image.shape[:2])

  def test_resize_image_to_max_box_with_same_width_but_to_less_height_room(self):
    image = np.zeros(shape=(50, 100, 3), dtype=np.uint8)
    new_image = app.resize_image_to_max_box(image, 100, 30, border=False)
    self.assertEqual((30, 60), new_image.shape[:2])

  def test_resize_image_to_max_box_to_ratio_greater_than_one(self):
    image = np.zeros(shape=(100, 10, 3), dtype=np.uint8)
    new_image = app.resize_image_to_max_box(image, 150, 150, ratio=1.5, border=False)
    self.assertEqual((150, 22), new_image.shape[:2])

  def test_resize_image_to_max_box_to_ratio_less_than_one(self):
    image = np.zeros(shape=(100, 12, 3), dtype=np.uint8)
    new_image = app.resize_image_to_max_box(image, 150, 150, ratio=0.5, border=False)
    self.assertEqual((150, 9), new_image.shape[:2])

  def test_resize_image_to_max_box_to_ratio_greater_than_one_and_init_size_to_big(self):
    image = np.zeros(shape=(100, 12, 3), dtype=np.uint8)
    new_image = app.resize_image_to_max_box(image, 50, 50, ratio=1.5, border=False)
    self.assertEqual((50, 9), new_image.shape[:2])

  def test_add_image_to_image_with_no_image(self):
    overlay_image = np.ones(shape=(40, 30, 3), dtype=np.uint8)
    bounds = Bounds(x=10, y=10, height=40, width=30)
    try:
      app.add_image_to_image(None, overlay_image, bounds)
      self.fail("Expected ValueError but got none")
    except ValueError as e:
      pass

  def test_add_image_to_image_with_no_base_image(self):
    base_image = np.zeros(shape=(100, 100, 3), dtype=np.uint8)
    bounds = Bounds(x=10, y=10, height=40, width=30)
    try:
      app.add_image_to_image(base_image, None, bounds)
      self.fail("Expected ValueError but got none")
    except ValueError as e:
      pass

  def test_add_image_to_image_with_no_bounds(self):
    base_image = np.zeros(shape=(100, 100, 3), dtype=np.uint8)
    overlay_image = np.ones(shape=(40, 30, 3), dtype=np.uint8)
    try:
      app.add_image_to_image(base_image, overlay_image, None)
      self.fail("Expected ValueError but got none")
    except ValueError as e:
      pass

  def test_add_image_to_image_with_base_image_out_of_bounds(self):
    base_image = np.zeros(shape=(100, 100, 3), dtype=np.uint8)
    overlay_image = np.ones(shape=(200, 30, 3), dtype=np.uint8)
    bounds = Bounds(x=10, y=10, height=40, width=30)
    try:
      app.add_image_to_image(base_image, overlay_image, bounds)
      self.fail("Expected ValueError but got none")
    except ValueError as e:
      pass

  def test_add_image_to_image_with(self):
    base_image = np.zeros(shape=(100, 100, 3), dtype=np.uint8)
    overlay_image = np.ones(shape=(40, 30, 3), dtype=np.uint8)
    bounds = Bounds(x=10, y=15, height=40, width=30)

    # when
    app.add_image_to_image(base_image, overlay_image, bounds)

    # then
    self.assertCountEqual([0, 0, 0], base_image[0][0])
    self.assertCountEqual([0, 0, 0], base_image[14][9])
    self.assertCountEqual([1, 1, 1], base_image[15][10])
    self.assertCountEqual([1, 1, 1], base_image[54][39])
    self.assertCountEqual([0, 0, 0], base_image[56][41])
