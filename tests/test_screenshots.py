import unittest

import screenshots
from screenshots import ScreenshotService, Screenshot, ScreenshotType


class ScreenshotTest(unittest.TestCase):

  def setUp(self) -> None:
    self.screenshot_service = ScreenshotService()
    self.screenshot_service.start_timer = lambda: None
    screenshots.urllib.request.urlopen = lambda url: None
    screenshots.urllib.request.urlretrieve = lambda url, location: None

  def test_screenstho_type_enum(self):
    self.assertEqual(ScreenshotType.UNKNOWN, ScreenshotType['UNKNOWN'])
    self.assertEqual(ScreenshotType.BLACK, ScreenshotType['BLACK'])

  def test_is_new_screenshot_available_not_initialized_and_not_available(self):
    self.screenshot_service.current_screenshot = None
    self.screenshot_service.new_screenshot = None

    self.assertFalse(self.screenshot_service.is_new_screenshot_available())

  def test_is_new_screenshot_available_not_initialized_and_available(self):
    self.screenshot_service.current_screenshot = None
    self.screenshot_service.new_screenshot = Screenshot(1, "", ScreenshotType.UNKNOWN)

    self.assertTrue(self.screenshot_service.is_new_screenshot_available())

  def test_is_new_screenshot_available_not_available(self):
    self.screenshot_service.current_screenshot = Screenshot(1, "", ScreenshotType.UNKNOWN)
    self.screenshot_service.new_screenshot = Screenshot(1, "", ScreenshotType.UNKNOWN)

    self.assertFalse(self.screenshot_service.is_new_screenshot_available())

  def test_is_new_screenshot_available_available(self):
    self.screenshot_service.current_screenshot = Screenshot(1, "", ScreenshotType.UNKNOWN)
    self.screenshot_service.new_screenshot = Screenshot(2, "", ScreenshotType.UNKNOWN)

    self.assertTrue(self.screenshot_service.is_new_screenshot_available())

  def test_get_screenshot_current_on_init(self):
    self.screenshot_service.current_screenshot = None
    self.screenshot_service.new_screenshot = None

    self.assertIsNone(self.screenshot_service.get_screenshot())

  def test_get_screenshot_current(self):
    self.screenshot_service.current_screenshot = None
    self.screenshot_service.new_screenshot = None

    self.assertEqual(self.screenshot_service.current_screenshot, self.screenshot_service.get_screenshot())

  def test_fetch_new_screenshot_success_on_init(self):
    class Mockresponse:
      def read(self):
        return b'{"id":2,"type":"INFORMATION"}'

    def urlretrieve_assert(url):
      self.assertEqual(screenshots.settings.screenshots_info_URL, url)
      return Mockresponse()

    screenshots.settings.screenshots_media_URL = "http://myurl.com/{id}"
    screenshots.urllib.request.urlopen = lambda url: urlretrieve_assert(url)
    self.screenshot_service.is_online = False
    self.screenshot_service.current_screenshot = None
    self.screenshot_service.new_screenshot = None

    # when
    self.screenshot_service.fetch_new_screenshot()

    # then
    self.assertTrue(self.screenshot_service.is_online)
    self.assertEqual(2, self.screenshot_service.new_screenshot.id)
    self.assertEqual(self.screenshot_service.new_screenshot_location, self.screenshot_service.new_screenshot.location)
    self.assertEqual(ScreenshotType.INFORMATION, self.screenshot_service.new_screenshot.type)

  def test_fetch_new_screenshot_success(self):
    class Mockresponse:
      def read(self):
        return b'{"id":2,"type":"INFORMATION"}'

    def urlretrieve_assert(url):
      self.assertEqual(screenshots.settings.screenshots_info_URL, url)
      return Mockresponse()

    screenshots.settings.screenshots_media_URL = "http://myurl.com/{id}"
    screenshots.urllib.request.urlopen = lambda url: urlretrieve_assert(url)
    self.screenshot_service.is_online = False
    self.screenshot_service.current_screenshot = Screenshot(1, "", ScreenshotType.INFORMATION)
    self.screenshot_service.new_screenshot = None

    # when
    self.screenshot_service.fetch_new_screenshot()

    # then
    self.assertTrue(self.screenshot_service.is_online)
    self.assertEqual(2, self.screenshot_service.new_screenshot.id)
    self.assertEqual(self.screenshot_service.new_screenshot_location, self.screenshot_service.new_screenshot.location)
    self.assertEqual(ScreenshotType.INFORMATION, self.screenshot_service.new_screenshot.type)

  def test_fetch_new_screenshot_success_but_not_unique(self):
    class Mockresponse:
      def read(self):
        return b'{"id":2,"type":"INFORMATION"}'

    def urlretrieve_assert(url):
      self.assertEqual(screenshots.settings.screenshots_info_URL, url)
      return Mockresponse()

    screenshots.settings.screenshots_media_URL = "http://myurl.com/{id}"
    screenshots.urllib.request.urlopen = lambda url: urlretrieve_assert(url)
    self.screenshot_service.is_online = False
    self.screenshot_service.current_screenshot = Screenshot(2, "", ScreenshotType.INFORMATION)
    self.screenshot_service.new_screenshot = None

    # when
    self.screenshot_service.fetch_new_screenshot()

    # then
    self.assertTrue(self.screenshot_service.is_online)
    self.assertIsNone(self.screenshot_service.new_screenshot)

  def test_fetch_new_screenshot_failure(self):
    def urlretrieve_assert(url):
      raise Exception

    screenshots.settings.screenshots_media_URL = "http://myurl.com/{id}"
    screenshots.urllib.request.urlopen = lambda url: urlretrieve_assert(url)
    self.screenshot_service.is_online = True
    self.screenshot_service.new_screenshot = None

    # when
    self.screenshot_service.fetch_new_screenshot()

    # then
    self.assertFalse(self.screenshot_service.is_online)
    self.assertIsNone(self.screenshot_service.new_screenshot)

  def test_fetch_new_screenshot_failure_on_download(self):
    class Mockresponse:
      def read(self):
        return b'{"id":2,"type":"INFORMATION"}'

    def urlretrieve_assert(url):
      self.assertEqual(screenshots.settings.screenshots_info_URL, url)
      return Mockresponse()

    def urlretrieve_assert2(url, location):
      raise Exception

    screenshots.settings.screenshots_media_URL = "http://myurl.com/{id}"
    screenshots.urllib.request.urlopen = lambda url: urlretrieve_assert(url)
    screenshots.urllib.request.urlretrieve = lambda url, location: urlretrieve_assert2(url, location)
    self.screenshot_service.is_online = True
    self.screenshot_service.current_screenshot = Screenshot(1, "", ScreenshotType.INFORMATION)
    self.screenshot_service.new_screenshot = None

    # when
    self.screenshot_service.fetch_new_screenshot()

    # then
    self.assertFalse(self.screenshot_service.is_online)
    self.assertIsNone(self.screenshot_service.new_screenshot)

  def test_download_screenshot_success(self):
    def urlretrieve_assert(url, location):
      self.assertEqual("http://myurl.com/2", url)
      self.assertEqual(self.screenshot_service.new_screenshot_location, location)

    screenshots.settings.screenshots_media_URL = "http://myurl.com/{id}"
    screenshots.urllib.request.urlretrieve = lambda url, location: urlretrieve_assert(url, location)
    self.screenshot_service.is_online = False

    # when
    self.assertTrue(self.screenshot_service.download_screenshot(2))
    self.assertTrue(self.screenshot_service.is_online)

  def test_download_screenshot_failure(self):
    def urlretrieveAssert(url, location):
      raise Exception

    screenshots.settings.screenshots_media_URL = "http://myurl.com/{id}"
    screenshots.urllib.request.urlretrieve = lambda url, location: urlretrieveAssert(url, location)
    self.screenshot_service.is_online = True

    # when
    self.assertFalse(self.screenshot_service.download_screenshot(2))
    self.assertFalse(self.screenshot_service.is_online)
