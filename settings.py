import logging
import os

#
# VIDEO STREAM

# Connection settings for the stream. These settings will be inserted in the video stream URL
stream_connection_settings = {
  "user": "myusername",  # De gebruikersnaam
  "password": "password123",  # Het wachtwoord
  "ip": "192.168.178.100",  # Het IP-adres van de camera
  "port": "554",  # De RTSP poort van de camera
  "channel": "1",  # Het weer te geven kanaal, bij de meeste camera’s kan dit alleen 1 zijn
  "stream": "0",  # De weer te geven stream, 0 voor Mainstream en 1 voor Substream
}

# Stream URL to get the video from. The stream_connection_settings will be inserted in this url
streamURL = "rtsp://{user}:{password}@{ip}:{port}/cam/realmonitor?channel={channel}&subtype={stream}"
# streamURL = "rtsp://freja.hiof.no:1935/rtplive/_definst_/hessdalen03.stream"  # Test stream

stream_video_desired_ratio = 1.333

output_format = {
  "x": 0,  # Set to the first monitor's width to display on the second monitor
  "y": 0,
  "width": 1280,
  "height": 720,
}

#
# VIDEO OVERLAY

# Display a screenshot image overlay over the video stream
add_screenshot_overlay = True
overlay_settings = {
  "x": 20,
  "y": 10,
  "width": 470,
  "height": 323
}

# Interval in seconds (float) for which new screenshots must be fetched
screenshots_update_interval = 0.5
screenshots_info_URL = "http://localhost:8080/api/screenshots/latest"
# URL to download the new screenshot image from
screenshots_media_URL = "http://localhost:8080/media/screenshots/{id}"
# Local path to store the current displayed screenshot image (new screenshot images will be stored elsewhere)
screenshots_local_location = "media/screenshot_local.png"

#
# DOWN TIME

# Time in seconds (float) to wait while the video stream is down, before any actions will be taken
max_downtime_timeout = 2.0

#
# APPLICATION

LOGLEVEL = 'INFO'

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

# Loading local settings
localsettingspath = os.path.join(BASE_DIR, "settings_local.py")
if os.path.exists(localsettingspath):
  from settings_local import *

# Setting logging settings
logging.basicConfig(
  format='[%(levelname)s] %(asctime)s %(name)s {%(module)s} | %(message)s',
  level=LOGLEVEL)
