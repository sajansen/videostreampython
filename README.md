# videostreampython

_Capture RTSP video stream, modify it (by adding Screenshot layer), display it._


**Requires**: [ScreenChangeNotifierServer](https://bitbucket.org/sajansen/screenchangenotifierserver/src) for serving the screenshots:

> `screenshots.py` does a request to the server on `settings.screenshots_info_URL` to fetch an object with a unique `id` field, every `settings.screenshots_update_interval`. Next, `screenshots.py` will download the corresponding file from `settings.screenshots_media_URL` to `ScreenshotService.new_screenshot_location`. When a new video frame is being edited, `app.handle_video_frame()` will make a request to `ScreenshotService.get_screenshot_location()` in order to load the screenshot to display over the frame. This function will check if a new screenshot was downloaded and if so, it will load it into `settings.screenshots_local_location`.


## Install

##### Requirements

It requires Python 3.6 or greater.


```bash
python -m venv venv
. venv/bin/activate
pip install -r requirements
``` 

#### Raspberry PI

Raspberry PI is a different one. You need to install extra packages:
```bash
sudo apt-get install libcblas-dev
sudo apt-get install libhdf5-dev
sudo apt-get install libhdf5-serial-dev
sudo apt-get install libatlas-base-dev
sudo apt-get install libjasper-dev 
sudo apt-get install libqtgui4 
sudo apt-get install libqt4-test
```

Source: https://stackoverflow.com/questions/53347759/importerror-libcblas-so-3-cannot-open-shared-object-file-no-such-file-or-dire


## Usage

Rename `settings_local.py.example` to `settings_local.py` and modify your specific settings, or just modify `settings.py` directly (not recommend for passwords).

Fire up the app:

```bash
python app.py
```

#### Output streaming

To stream the output over RTSP again, execute: 

```bash
./start_output_stream.sh`.
``` 

This will start the python app (as above) but will redirect the output to a stream. The settings for this stream are in the same `start_output_stream.sh` file. These settings are the streaming IP address and port number.

On the receiving and, troubles with not being able to open H264 encoding can be solved with: `sudo apt install gstreamer1.0-libav`.